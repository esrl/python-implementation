FROM python:3.8-alpine3.10

# Install supervisord for quick restart of python script
RUN apk add supervisor

# Install python dependencies
COPY requirements.txt .
RUN pip install -r requirements.txt 

# Copy scripts to container
COPY scripts scripts

# Copy supervisord to container
COPY supervisord.conf /etc/supervisord.conf

# Get commit hash from Gitlab CI
ARG COMMIT_SHA 
ENV X_COMMIT_SHA=$COMMIT_SHA

# Run when container is created
#CMD python3.8 -u /scripts/main.py
CMD ["/usr/bin/supervisord"]
