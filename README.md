### python-implementation

Start docker container with `scripts/run.py`
Run `scripts/global_controller.py` from any machine on the network to configure

Listen to spikes coming from NeuroProgram:
`kafkacat -o end -b cloudbraink01 -t spike_from_1`

Send spike to NeuroProgram
`echo "{\"@timestamp\":\"$(date -u +%Y-%m-%dT%T.%N)\",\"sender\":\"spike_from_01\",\"payload\":{\"name\":\"Leon\", \"state\":\"drunk\"}}" | kafkacat  -P -b cloudbraink01 -t spike_from_1`

Listen to control events:
`kafkacat -o end -b cloudbraink01 -t control_to_1`



