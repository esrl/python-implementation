#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

for _id in {200..200}
do  
    docker run -e X_SLOT=$_id -v $DIR/scripts:/scripts -it --net=host --name=cb_node_$_id --rm cloud_brain:latest python3.8 -u /scripts/main.py
done
