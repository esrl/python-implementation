#!/usr/bin/env python3.6
# -*- coding: utf-8 -*-
import os
from neuro_programs.neuro_programs import *
import threading, time, json, collections
from kafka import KafkaConsumer, KafkaProducer
from datetime import datetime

class KafkaConsumerThread(threading.Thread):
    def __init__(self, servers, name, receive_callback=None):
        # Init thread
        threading.Thread.__init__(self)
        self.stop_event = threading.Event()
     
        # Class variables
        self.servers = servers
        self.receive_callback = receive_callback
        self.name = name

        # Init consumer
        self.consumer = KafkaConsumer(bootstrap_servers=self.servers,auto_offset_reset='latest',consumer_timeout_ms=1000)
        
        # Prepare topics
        self.spike_to_topic = 'spike_to_' + self.name
        self.subscribed_topics = []
        self.subscribe(self.spike_to_topic)

    def stop(self):
        self.stop_event.set()

    def run(self):
        while not self.stop_event.is_set():
            for message in self.consumer:
                event = json.loads(message.value)
                self.receive_callback(event)
     
            if self.stop_event.is_set():
                break

        self.consumer.close()
    
    def subscribe(self, topic):
        self.subscribed_topics.append(topic)
        self.consumer.subscribe(self.subscribed_topics)

    def unsubscribe(self, topic):
        self.subscribed_topics.remove(topic)
        self.consumer.subscribe(self.subscribed_topics)

    def reset(self):
        self.subscribed_topics = []
        self.subscribe(self.spike_to_topic)

class ControlProgram(threading.Thread):
    def __init__(self, name, kafka_broker):
        threading.Thread.__init__(self)
        self.stop_event = threading.Event()
        
        self.log_parameters = True

        self.name = str(name)
	
        self.servers = kafka_broker
        self.neuro_programs = []

        self.connection_info = dict()

        self.timer_creation_allowed = True

        self.max_number_of_timers = 10
        self.timer_handles = collections.deque(maxlen=self.max_number_of_timers)
        self.spike_topic = 'spike_from_' + self.name

        self.control_consumer = KafkaConsumerThread(servers=self.servers, name = self.name, receive_callback=self.controlReceived)
        self.control_consumer.subscribe('control_to_' + self.name)
        self.spike_consumer = KafkaConsumerThread(servers=self.servers, name = self.name, receive_callback=self.spikeReceived)
        self.spike_producer = KafkaProducer(bootstrap_servers=self.servers)
   
        self.action_to_method = {
                'subscribe':self.subscribe,
                'unsubscribe':self.unsubscribe,
                'set_parameters':self.setNeuroProgramParameters,
                'get_parameters':self.getNeuroProgramParameters,
                'add_neuro_program':self.addNeuroProgram,
                'reset_neuro_programs':self.resetNeuroPrograms
                }

    def controlReceived(self, event):
        print(self.name + " - CP: Received control event from ", event['sender'])
        #print(event)
        method = self.action_to_method[ event['payload']['action'] ]
        method( event )

    def addNeuroProgram(self, event):
        np_id = event['payload']['neuro_program']

        if False:
            pass

        elif np_id == 'NoisySpikeSource':
            print(self.name + " - CP: Adding neuron model ", np_id)
            self.addNP( NoisySpikeSource(_id = event['payload']['id']) )
        elif np_id == 'IFNeuron' or np_id == 'IntegrateAndFireNeuron':
            print(self.name + " - CP: Adding neuron model ", np_id)
            self.addNP( IFNeuron(_id = event['payload']['id']) )
        elif np_id == 'SyncIFNeuron' or np_id == 'SynchronousIntegrateAndFireNeuron':
            print(self.name + " - CP: Adding neuron model ", np_id)
            self.addNP( SyncIFNeuron(_id = event['payload']['id']) )
        elif np_id == 'LIFNeuron':
            print(self.name + " - CP: Adding neuron model ", np_id)
            self.addNP( LIFNeuron(_id = event['payload']['id']) )
        elif np_id == 'SyncLIFNeuron':
            print(self.name + " - CP: Adding neuron model ", np_id)
            self.addNP( SyncLIFNeuron(_id = event['payload']['id']) )
        elif np_id == 'AdExNeuron':
            print(self.name + " - CP: Adding neuron model ", np_id)
            self.addNP( AdExNeuron(_id = event['payload']['id']) )
        else:
            print(self.name + " - CP: Unknown neuron model ", np_id)
        """
        elif np_id == 'TestNeuron':
            print(self.name + " - CP: Adding neuron model ", np_id)
            self.addNP( TestNeuron() )
        elif np_id == 'RelayNeuron':
            print(self.name + " - CP: Adding neuron model ", np_id)
            self.addNP( RelayNeuron() )
        elif np_id == 'FrobitNeuron':
            print(self.name + " - CP: Adding neuron model ", np_id)
            self.addNP( FrobitNeuron() )
        elif np_id == 'TargetIntegrateAndFireNeuron':
            print(self.name + " - CP: Adding neuron model ", np_id)
            self.addNP( IFNeuron(_id=event['payload']['id']) )
        elif np_id == 'SimpleNeuron':
            print(self.name + " - CP: Adding neuron model ", np_id)
            self.addNP( SimpleNeuron() )
        """

    def addNP(self, neuro_program):
        self.neuro_programs.append( neuro_program )
        neuro_program.registerTimer = self.registerTimer
        neuro_program.sendSpike = self.sendSpike

    def resetNeuroPrograms(self, event):
        print(self.name + " - CP: Restarting container application")
        os._exit(1)
        """
        print("CP: Resetting neuro programs")
        self.timer_creation_allowed = False
        self.neuro_programs = []
        #self.spike_consumer.stop()
        #self.spike_consumer = KafkaConsumerThread(servers=self.servers, name = self.name, receive_callback=self.spikeReceived)
        #self.spike_consumer.start()
        for timer in self.timer_handles:
            timer.cancel()
        self.timer_handles.clear()
        self.timer_creation_allowed = True
        print("CP: Unsubscribing from all topics")
        self.spike_consumer.consumer.unsubscribe()
        """

    def subscribe(self, event):
        print(self.name + " - CP: Subscribing to " + event['payload']['topic'])
        self.connection_info[event['payload']['topic']] = event['payload']['connection_info']
        self.spike_consumer.subscribe(event['payload']['topic'])
    
    def unsubscribe(self, event):
        print(self.name + " - CP: Unsubscribing to " + event['payload']['topic'])
        del self.connection_info[event['payload']['topic']]
        self.spike_consumer.subscribe(event['payload']['topic'])

    def setNeuroProgramParameters(self, event):
        print(self.name + " - CP: Setting parameters")
        for neuro_program in self.neuro_programs:
            neuro_program.setParameters(event['payload'])

    def getNeuroProgramParameters(self, event):
        pass

    def stop(self):
        self.stop_event.set()

    def run(self):
        self.control_consumer.start()
        self.spike_consumer.start()
        while not self.stop_event.is_set():
            if len(self.neuro_programs) > 0:
                for neuro_program in self.neuro_programs:
                    neuro_program.loopForever() # TODO: Implement in different threads
                    if self.log_parameters:
                        ts = datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%S.%f")
                        msg = json.dumps({"@timestamp": ts, "parameters": neuro_program.parameters})
                        self.spike_producer.send("parameters_from_" + self.name, bytes(msg, 'utf-8'))
                        if "voltage_potential" in neuro_program.parameters:
#                            print(neuro_program.parameters["voltage_potential"])
                            pass
            else:
                print(self.name + " - CP: Waiting for neuron model to be added.")
                time.sleep(1.0)
            if self.stop_event.is_set():
                break

    def spikeReceived(self, event):
        sender = event['sender']
        if sender in self.connection_info.keys():
            #if sender in self.connection_info.keys():
                #print("CP: Sender match", sender)
            if not sender in self.connection_info:
                print(self.name + " - CP: Received spike from unconnected neuron")
            else:
                event['payload']['connection_info'] = self.connection_info[sender]
                for neuro_program in self.neuro_programs:
                    neuro_program.spikeReceived( event )
        else:
            #print(self.name + " - CP: Dropping spike from unknown topic ", str(event))
            pass

    def registerTimer(self, delay, callback, info, ts=None): # New version. Use of ts has changed. Sorry for not making it backwards compatible...
        if ts is not None:
            now = datetime.utcnow()
            if ts > now: # There is still time
                delay = (ts - datetime.utcnow()).total_seconds()
            else:
                delay = 0
        if self.timer_creation_allowed:
            timer = threading.Timer(delay, callback, kwargs=info)
            self.timer_handles.append(timer)  # TODO: check if discarded elements have fired = .finished.isSet()
            timer.start()

#    def registerTimer(self, delay, callback, info, ts=None):
#        if ts is not None:
#            delay -= (datetime.utcnow() - ts).total_seconds() 
#        if self.timer_creation_allowed:
#            timer = threading.Timer(delay, callback, kwargs=info)
#            self.timer_handles.append(timer)  # TODO: check if discarded elements have fired = .finished.isSet()
#            timer.start()

    def sendSpike(self, event, topic = None):
        if topic is None:
            topic = self.spike_topic
        event['sender'] = topic
        print('CP: sending spike on ', event['sender'])
        msg = json.dumps(event)
        self.spike_producer.send( topic, bytes(msg, 'utf-8'))
        self.spike_producer.flush()
