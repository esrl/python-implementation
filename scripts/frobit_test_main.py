#!/usr/bin/env python
# -*- coding: utf-8 -*-

from NeuroPrograms import *
from ControlProgram import *

if __name__ == "__main__":
    print("Main: Initialising controller")
    cp = ControlProgram(name='00')
    cp.addNeuroProgram( FrobitNeuron(id='left') )
    cp.addNeuroProgram( FrobitNeuron(id='right') )
    cp.start()
    time.sleep(2.0)
