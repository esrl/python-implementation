#!/usr/bin/env python
# -*- coding: utf-8 -*-

from ControlProgram import *
from os import environ, getenv

if __name__ == "__main__":

    name = None
    if "X_SLOT" in environ:
        name = getenv('X_SLOT')

    if "HOSTNAME" in environ:
        if "-" in getenv('HOSTNAME'):
            name = getenv('HOSTNAME').split("-")[1]

    if name == None:
        exit("ERROR: Environment variable \"HOSTNAME\" OR \"X_SLOT\" is not defined in env. Either one should be set for uniq identifier of neuron")

     
   


    # X_COMMIT_SHA is set by Gitlab during build
    sha_commit = getenv('X_COMMIT_SHA') if ("X_COMMIT_SHA" in environ and getenv('X_COMMIT_SHA') != "") else "NA"

    kafka_broker = getenv('X_KAFKA_BROKER') if ("X_KAFKA_BROKER" in environ and getenv('X_KAFKA_BROKER') != "") else "kafka"

    print("Main: Initialising controller: {}".format(name))
    print("Main: Starting git commit: {}".format(sha_commit))
    print("Main: Using kafka broker: {}".format(kafka_broker))

    cp = ControlProgram(name=name, kafka_broker=kafka_broker)
    cp.start()
    time.sleep(2.0)
