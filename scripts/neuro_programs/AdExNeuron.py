#!/usr/bin/env python
# -*- coding: utf-8 -*-

#import time
#from datetime import timedelta
#from datetime import datetime
#from random import random, uniform

from neuro_programs.NeuroProgram import NeuroProgram

from math import exp as exp

import time
from datetime import timedelta
from datetime import datetime
from random import random, uniform

class AdExNeuron(NeuroProgram):
    def __init__(self, _id = 0):
        print("NP: Starting \"Adaptive exponential integrate and fire Neuron\"")
        NeuroProgram.__init__(self, _id)
        self.id = _id
        self.parameters = {"voltage_potential": -0.070, "refrac": 0.0, "rest": -0.070, "b": 100.0, "V": -0.070, "w": 0, "I": 0.0, "C": 200.0, "g_L": 10.0, "E_L": -0.058, "V_T": -0.055, "D_T": 2.0, "a": 2.0, "T_w": 120.0}
        self.last_ts = datetime.utcnow()

    def evaluateNeuron(self, **info):
        self.parameters["I"] = self.parameters["I"] + info["weight"]
    
    def spikeReceived(self, event):
#        print("NP: Spike received. ", str(event['payload']['connection_info']))
        #ts = datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%f")
        ts = datetime.strptime(event['@timestamp'][:26], '%Y-%m-%dT%H:%M:%S.%f')
        delay = event["payload"]["connection_info"]["delay"]
        delay = timedelta(seconds = delay)
        ts += delay
        sleep_time = ts - datetime.utcnow()

        if (sleep_time.days >= 0):
            self.registerTimer(sleep_time.seconds + sleep_time.microseconds / 10e6, self.evaluateNeuron, {"weight": event["payload"]["connection_info"]["weight"], "input_arival_time": ts})
        else:
            print("Deadline passed!")
            self.evaluateNeuron(weight = event["payload"]["connection_info"]["weight"], input_arival_time = ts)

    def loopForever(self):
        if self.parameters["refrac"] > 0:
            dv = 0.0
            self.parameters["refrac"] -= 1.0
        else:
            dv = (-self.parameters["g_L"] * (self.parameters["V"] - self.parameters["E_L"]) + self.parameters["g_L"] * self.parameters["D_T"] * exp((self.parameters["V"] - self.parameters["V_T"]) / self.parameters["D_T"]) - self.parameters["w"] + self.parameters["I"]) / self.parameters["C"]
        dw = (self.parameters["a"] * (self.parameters["V"] - self.parameters["E_L"]) - self.parameters["w"]) / self.parameters["T_w"]
        self.parameters["I"] = 0.0

        self.parameters["V"] += dv
        self.parameters["w"] += dw

        if (self.parameters["V"] >= self.parameters["V_T"]):
            self.parameters["V"] -= self.parameters["V_T"] - self.parameters["rest"]
            self.parameters["w"] += self.parameters["b"]
            self.parameters["refrac"] = 2.0
            spike = dict()
            spike['@timestamp'] = datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%S.%f")
            spike['payload'] = dict()
            if "population" in self.parameters:
                spike['payload']['population'] = self.parameters['population']
            self.sendSpike(spike)

        self.parameters["voltage_potential"] = self.parameters["V"]

        print(self.parameters["V"], self.parameters["w"])

        time.sleep(0.001)
