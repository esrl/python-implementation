#!/usr/bin/env python
# -*- coding: utf-8 -*-
#import time
#from datetime import timedelta
#from datetime import datetime
#from random import random, uniform

from neuro_programs.NeuroProgram import NeuroProgram

import time
from datetime import timedelta
from datetime import datetime
from random import random, uniform

class IFNeuron(NeuroProgram):
    def __init__(self, _id = 0):
        print("NP: Starting \"Integrate And Fire Neuron\"")
        NeuroProgram.__init__(self, _id)
        self.id = _id
        self.parameters = {"voltage_potential": -70.0, "rest_potential": -70.0, "threshold": -55.0}
        self.last_ts = datetime.utcnow()

    def evaluateNeuron(self, **info):
        self.parameters["voltage_potential"] += info["weight"]
        print("Sending spik")
        if (self.parameters["voltage_potential"] >= self.parameters["threshold"]):
            spike = dict()
            spike['@timestamp'] = info["input_arival_time"].strftime("%Y-%m-%dT%H:%M:%S.%f")
            spike['payload'] = dict()
            if "population" in self.parameters:
                spike['payload']['population'] = self.parameters['population']
            self.sendSpike(spike)
            self.parameters["voltage_potential"] -= self.parameters["threshold"] - self.parameters["rest_potential"]
        
        elif (self.parameters["voltage_potential"] < self.parameters["rest_potential"]):
            self.parameters["voltage_potential"] = self.parameters["rest_potential"]
    
    def spikeReceived(self, event):
#        print("NP: Spike received. ", str(event['payload']['connection_info']))
        timestamp = datetime.strptime(event['@timestamp'][:26], '%Y-%m-%dT%H:%M:%S.%f')
        delay = timedelta(seconds = event["payload"]["connection_info"]["delay"])
        timestamp += delay
        sleep_time = timestamp - datetime.utcnow()

        if (sleep_time.days >= 0):
            self.registerTimer(sleep_time.seconds + sleep_time.microseconds / 10e6, self.evaluateNeuron, {"weight": event["payload"]["connection_info"]["weight"], "input_arival_time": timestamp})
        else:
            print("Deadline passed!")
            self.evaluateNeuron(weight = event["payload"]["connection_info"]["weight"], input_arival_time = timestamp)
