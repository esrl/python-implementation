#!/usr/bin/env python
# -*- coding: utf-8 -*-

from neuro_programs.NeuroProgram import NeuroProgram

import time
from datetime import timedelta
from datetime import datetime
from random import random, uniform

class LIFNeuron(NeuroProgram):
    def __init__(self, _id = 0):
        print("NP: Started \"Leaky Integrate And Fire Neuron2\"")
        NeuroProgram.__init__(self, _id)
        self.id = _id
        self.parameters = {"voltage_potential": -0.070, "rest_potential": -0.070, "threshold": -0.055, "noise_gain": 0.1, "decay": 0.1}
        self.spike = dict()
        self.spike['payload'] = dict()
        self.prev_time = datetime.utcnow()

    def evaluateNeuron(self, **event):
        print(f'NP {self.id}: evaluating neuron')
        spike_time = event["payload"]["spike_time"]
        lapsed = (spike_time - self.prev_time).total_seconds() * 1000
        print("Lapsed:", lapsed)
        self.parameters["voltage_potential"] = self.parameters["rest_potential"] + (self.parameters["voltage_potential"] - self.parameters["rest_potential"]) * (self.parameters["decay"] ** lapsed)
        self.parameters["voltage_potential"] += event["payload"]["connection_info"]["weight"]
        if (self.parameters["voltage_potential"] >= self.parameters["threshold"]):
            self.spike['@timestamp'] = spike_time.strftime("%Y-%m-%dT%H:%M:%S.%f")
            if "population" in self.parameters:
                self.spike['payload']['population'] = self.parameters['population']
            self.parameters["voltage_potential"] -= self.parameters["threshold"] - self.parameters['rest_potential']
            self.sendSpike(self.spike)
            print("Spike")
        self.prev_time = spike_time
        #print(f'NP {self.id}: VP = {self.parameters["voltage_potential"]}')
        
    def spikeReceived(self, event):
        #print(f'NP {self.id}: receiving spike')
        timestamp = datetime.strptime(event['@timestamp'][:26], '%Y-%m-%dT%H:%M:%S.%f')
        delay = timedelta(seconds = event["payload"]["connection_info"]["delay"])# + random() * self.parameters["noise_gain"] )
        spike_time = timestamp + delay
        event["payload"]["spike_time"] = spike_time
        
        print("register timer")
        self.registerTimer(delay=None, callback=self.evaluateNeuron, info=event, ts=spike_time)
        if ((spike_time - datetime.utcnow()).days < 0): 
            print("Deadline passed!")
