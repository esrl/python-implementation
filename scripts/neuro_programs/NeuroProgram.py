#!/usr/bin/env python
# -*- coding: utf-8 -*-
import time
from datetime import timedelta
from datetime import datetime
from random import random, uniform

class NeuroProgram(object):
    # Services:
    #   sendSpike( event ) ; event is a dict
    #   registerTimer( dt, callback, info ) ; dt is time in seconds, callback is function to call when timer expires, info is kwargs for callback (dict)
    # Functionalities (override):
    #   spikeReceived(connection_info) ; connection_info is dict
    #   loopForever() ; called continuously. Use sleep to control period
    def __init__(self, id = None):
        self.registerTimer = None
        self.sendSpike = None
        self.parameters = dict
        self.id = id

    def setParameters(self, parameters):
        print(f"NP {self.id}: Setting parameters to {parameters}")
        for key, val in parameters.items():
            self.parameters[key] = val

    def spikeReceived(self, event):
        pass

    def loopForever(self):
        time.sleep(0.001)
