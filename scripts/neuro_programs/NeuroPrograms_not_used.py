#!/usr/bin/env python
# -*- coding: utf-8 -*-
import time
from datetime import timedelta
from datetime import datetime
from random import random, uniform

class NeuroProgram(object):
    # Services:
    #   sendSpike( event ) ; event is a dict
    #   registerTimer( dt, callback, info ) ; dt is time in seconds, callback is function to call when timer expires, info is kwargs for callback (dict)
    # Functionalities (override):
    #   spikeReceived(connection_info) ; connection_info is dict
    #   loopForever() ; called continuously. Use sleep to control period
    def __init__(self):
        self.registerTimer = None
        self.sendSpike = None
        self.parameters = dict
        self.id = None

    def setParameters(self, parameters):
        #print(f"NP {self.id}: Setting parameters to {parameters['payload']}")
        self.parameters = parameters['payload']

    def spikeReceived(self, event):
        pass

    def loopForever(self):
        time.sleep(1.0)


class TestNeuron(NeuroProgram):
    def __init__(self):
        NeuroProgram.__init__(self)
        self.previous_spike_timestamp = datetime.utcnow()

    def setParameters(self, parameters):
        print(f"NP: Setting parameters to ", parameters)
        self.parameters = parameters

    def spikeReceived(self, event):
        #print("NP: Spike received. ", str(event['payload']['connection_info']))
        ts = datetime.strptime(event['@timestamp'][:26], '%Y-%m-%dT%H:%M:%S.%f') 
        dt = ts - self.previous_spike_timestamp
        #print("NP: Time since last spike: ", str(dt.total_seconds()), " seconds")
        self.previous_spike_timestamp = ts

    def timerCallback(self, **info):
        #print('NP: Timer callback. ', str(info))
        pass

    def loopForever(self):
        ts = datetime.utcnow()
        #print('NP: Looping')
        spike = dict()
        spike['@timestamp'] = datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%f")
        spike['payload'] = dict()
        self.sendSpike(spike)
        self.registerTimer(1.0, self.timerCallback, {'msg':'hello from the past'}, ts=ts)
        time.sleep(5.0)

class SimpleNeuron(NeuroProgram):
    def __init__(self):
        print('NP: Initialising Simple Neuron')
        NeuroProgram.__init__(self)
        self.spike = dict()
        self.spike['@timestamp'] = datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%f")
        self.spike['payload'] = dict()
        self.parameters = dict()
        self.parameters['potential'] = 0.0
        self.parameters['spike_value'] = 1.0
        self.parameters['fraction'] = 0.5
        self.parameters['period'] = 0.5
        self.parameters['bias'] = 1.0

    def spikeReceived(self, event):
        #print('NP: Spike received',event)
        self.parameters['potential'] += self.parameters['spike_value'] * event['payload']['connection_info']['weight']

    def loopForever(self):
        print('NP: loop',self.parameters['potential'])
        if self.parameters['potential'] >= self.parameters['bias']:
            print('NP: generating spike')
            self.spike['@timestamp'] = datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%f")
            self.sendSpike(self.spike)
            self.parameters['potential'] -= self.parameters['bias']
        self.parameters['potential'] *= self.parameters['fraction']
        if self.parameters['potential'] < 0.0001:
            self.parameters['potential'] = 0.0
        time.sleep(self.parameters['period'])


class FrobitNeuron(NeuroProgram):
    def __init__(self, id):
        NeuroProgram.__init__(self)
        self.spike = dict()
        self.spike['payload'] = dict()
        self.id = id
        self.initialised = False

    def setParameters(self, parameters):
        if parameters['id'] == self.id: 
            print("NP ",self.id, ": Setting parameters to ", parameters)
            self.parameters = parameters
            self.initialised = True

    def loopForever(self):
        if self.initialised:
            self.spike['@timestamp'] = datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%f")
            self.sendSpike(self.spike, topic=self.parameters['send_topic'])
        time.sleep(0.1)

class RelayNeuron(NeuroProgram):
    def __init__(self):
        print("NP: Relay neuron starting")
        self.id = id
        self.parameters = {
            "sleep_time": 0,
        }
        self.ts = datetime.utcnow()

    def setParameters(self, parameters):
        print("NP: RelayNeuron setting parameters to ", parameters)
        self.parameters = parameters
        self.ts = datetime.utcnow()
        #t = uniform(self.parameters["max_time"], self.parameters["min_time"])

    def spikeReceived(self, event):
        #time.sleep(self.parameters["sleep_time"])
        self.spike = dict()
        self.spike["payload"] = dict()
        self.spike["@timestamp"] = datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%f")
        #print("NP: RelayNeuron received spike:", self.spike["@timestamp"])
        self.sendSpike(self.spike)

class SpikeSource(NeuroProgram):
    def __init__(self):
        NeuroProgram.__init__(self)
        print("NP: SpikeSource starting")
        self.parameters = {"period": 0.1}
        self.spike = dict()
        spike['@timestamp'] = datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%f")
        spike['payload'] = dict()

    def setParameters(self, parameters):
        print("NP: SpikeSource setting parameters to ", parameters)
        self.parameters = parameters

    def loopForever(self):
        self.spike['@timestamp'] = datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%f")
        self.sendSpike(self.spike)
        time.sleep(self.parameters['period'])

class NoisySpikeSource(NeuroProgram):
    def __init__(self):
        print("NP: Noisy Spike Source starting")
        self.parameters = {"max_time": 0.1, "min_time": 0.001}
        self.ts = datetime.utcnow()

    def setParameters(self, parameters):
        print("NP: NoisySpikeSource setting parameters to ", parameters)
        self.parameters = parameters
        self.ts = datetime.utcnow()
        t = uniform(self.parameters["max_time"], self.parameters["min_time"])

    def spikeReceived(self, event):
        #print("NP: NoisySpikeSource ignoring received spike")
        pass

    def loopForever(self):
        
        delay = timedelta(seconds = uniform(self.parameters["max_time"], self.parameters["min_time"]))
        next_ts = self.ts + delay
        delta = next_ts - datetime.utcnow()
        if (delta.days == 0):
            time.sleep(delta.microseconds / 1000000 + delta.seconds)
        self.ts = next_ts
        spike = dict()
        spike['@timestamp'] = self.ts.strftime("%Y-%m-%dT%H:%M:%S.%f")
        spike['payload'] = dict()
        if "population" in self.parameters:
            spike['payload']['population'] = self.parameters['population']
        self.sendSpike(spike)

class IntegrateAndFireNeuron(NeuroProgram):
    def __init__(self):
        print("NP: Starting \"Integrate And Fire Neuron\"")
        NeuroProgram.__init__(self)
        self.parameters = {"voltage_potential": -70.0, "rest_potential": -70.0, "threshold": -55.0}
        self.last_ts = datetime.utcnow()

    def setParameters(self, parameters):
        print("NP: Setting parameters to ", parameters)
        self.parameters = parameters

    def evaluateNeuron(self, **info):
        #print("Spike action")
        if (self.parameters["voltage_potential"] + info["weight"] >= self.parameters["threshold"]):
            spike = dict()
            spike['@timestamp'] = info["input_arival_time"].strftime("%Y-%m-%dT%H:%M:%S.%f")
            spike['payload'] = dict()
            if "population" in self.parameters:
                spike['payload']['population'] = self.parameters['population']
            self.sendSpike(spike)
            self.parameters["voltage_potential"] -= self.parameters["threshold"] - self.parameters["rest_potential"]
        
        elif (self.parameters["voltage_potential"] + info["weight"] < self.parameters["rest_potential"]):
            self.parameters["voltage_potential"] = self.parameters["rest_potential"]
        else:
            self.parameters["voltage_potential"] += info["weight"]
    def loopForever(self):
        print("Hej")
        time.sleep(1)
    
    def spikeReceived(self, event):
#        print("NP: Spike received. ", str(event['payload']['connection_info']))
#        print("NP: Spike time:", event['@timestamp'][:26])
        #ts = datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%f")
        ts = datetime.strptime(event['@timestamp'][:26], '%Y-%m-%dT%H:%M:%S.%f')
        delay = event["payload"]["connection_info"]["delay"]
        delay = timedelta(seconds = delay)
        ts += delay
        sleep_time = ts - datetime.now()

        # Extract delay and weight
        #if (sleep_time.days >= 0):
#            print("timer callback")
        #    self.registerTimer(sleep_time.seconds + sleep_time.microseconds / 10e6, self.evaluateNeuron, {"weight": event["payload"]["connection_info"]["weight"], "input_arival_time": ts})
        #else:
#            print("direct call")
        self.evaluateNeuron(weight = event["payload"]["connection_info"]["weight"], input_arival_time = ts)

class IFNeuron(NeuroProgram):  # Integrate and fire
    def __init__(self, _id=0):
        NeuroProgram.__init__(self)
        self.id = _id
        self.parameters = {"voltage_potential": 0.0, "threshold": 1.0, "noise_gain": 0.1}
        self.spike = dict()
        self.spike['payload'] = dict()
        print(f"NP {self.id}: Started \"Integrate And Fire Neuron\"")

    def evaluateNeuron(self, **event):
        #print(f'NP {self.id}: evaluating neuron')
        self.parameters["voltage_potential"] += event["payload"]["connection_info"]["weight"]
        if (self.parameters["voltage_potential"] >= self.parameters["threshold"]):
            self.spike['@timestamp'] = event["payload"]["spike_time"].strftime("%Y-%m-%dT%H:%M:%S.%f")
            if "population" in self.parameters:
                spike['payload']['population'] = self.parameters['population']
            self.parameters["voltage_potential"] -= self.parameters["threshold"]
            self.sendSpike(self.spike)
        
    def spikeReceived(self, event):
        #print(f'NP {self.id}: receiving spike')
        timestamp = datetime.strptime(event['@timestamp'][:26], '%Y-%m-%dT%H:%M:%S.%f')
        #print(event)
        delay = timedelta(seconds = event["payload"]["connection_info"]["delay"] + random()*self.parameters["noise_gain"] )
        spike_time = timestamp + delay
        event["payload"]["spike_time"] = spike_time
        self.registerTimer(delay=None, callback=self.evaluateNeuron, info=event, ts=spike_time)

class LIFNeuron(NeuroProgram):
    def __init__(self, _id=0):
        NeuroProgram.__init__(self)
        self.id = _id
        self.parameters = {"voltage_potential": 0.0, "threshold": 1.0, "noise_gain": 0.1, "decay": 0.1}
        self.spike = dict()
        self.spike['payload'] = dict()
        self.prev_time = datetime.utcnow()
        print(f"NP {self.id}: Started \"Leaky Integrate And Fire Neuron\"")

    def evaluateNeuron(self, **event):
        #print(f'NP {self.id}: evaluating neuron')
        spike_time = event["payload"]["spike_time"]
        lapsed = (spike_time - self.prev_time).total_seconds()
        self.parameters["voltage_potential"] *= ((self.parameters["decay"])**lapsed)
        self.parameters["voltage_potential"] += event["payload"]["connection_info"]["weight"]
        if (self.parameters["voltage_potential"] >= self.parameters["threshold"]):
            self.spike['@timestamp'] = spike_time.strftime("%Y-%m-%dT%H:%M:%S.%f")
            if "population" in self.parameters:
                spike['payload']['population'] = self.parameters['population']
            self.parameters["voltage_potential"] -= self.parameters["threshold"]
            self.sendSpike(self.spike)
        self.prev_time = spike_time
        #print(f'NP {self.id}: VP = {self.parameters["voltage_potential"]}')
        
    def spikeReceived(self, event):
        #print(f'NP {self.id}: receiving spike')
        timestamp = datetime.strptime(event['@timestamp'][:26], '%Y-%m-%dT%H:%M:%S.%f')
        #print(event)
        delay = timedelta(seconds = event["payload"]["connection_info"]["delay"] + random()*self.parameters["noise_gain"] )
        spike_time = timestamp + delay
        event["payload"]["spike_time"] = spike_time
        self.registerTimer(delay=None, callback=self.evaluateNeuron, info=event, ts=spike_time)

    
#class TIFNeuron(NeuroProgram):
#class TLIFNeuron(NeuroProgram):

