#!/usr/bin/env python
# -*- coding: utf-8 -*-

from neuro_programs.NeuroProgram import NeuroProgram

import time
from datetime import timedelta
from datetime import datetime
from random import random, uniform

class PoissonSpikeSource(NeuroProgram):
    def __init__(self):
        print("NP: Poisson Spike Source starting")
        self.parameters = {"avg_time": 0.1}
        self.ts = datetime.utcnow()

    def setParameters(self, parameters):
        print("NP: NoisySpikeSource setting parameters to ", parameters)
        self.parameters = parameters
        self.ts = datetime.utcnow()
        t = uniform(self.parameters["max_time"], self.parameters["min_time"])

    def spikeReceived(self, event):
        #print("NP: NoisySpikeSource ignoring received spike")
        pass

    def loopForever(self):
        
        delay = timedelta(seconds = uniform(self.parameters["max_time"], self.parameters["min_time"]))
        next_ts = self.ts + delay
        delta = next_ts - datetime.utcnow()
        if (delta.days == 0):
            time.sleep(delta.microseconds / 1000000 + delta.seconds)
        self.ts = next_ts
        spike = dict()
        spike['@timestamp'] = self.ts.strftime("%Y-%m-%dT%H:%M:%S.%f")
        spike['payload'] = dict()
        if "population" in self.parameters:
            spike['payload']['population'] = self.parameters['population']
        self.sendSpike(spike)
