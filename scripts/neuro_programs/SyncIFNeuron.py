#!/usr/bin/env python
# -*- coding: utf-8 -*-
#import time
#from datetime import timedelta
#from datetime import datetime
#from random import random, uniform

from neuro_programs.NeuroProgram import NeuroProgram

import time
from datetime import timedelta
from datetime import datetime
from random import random, uniform

class SyncIFNeuron(NeuroProgram):
    def __init__(self, _id = 0):
        print("NP: Starting \"Synchronous Integrate And Fire Neuron\"")
        NeuroProgram.__init__(self, _id)
        self.id = _id
        self.parameters = {"voltage_potential": -0.070, "rest_potential": -0.070, "threshold": -0.055, "current": 0.0}
        self.last_ts = datetime.utcnow()

    def evaluateNeuron(self, **info):
        self.parameters["current"] = info["weight"]
    
    def spikeReceived(self, event):
#        print("NP: Spike received. ", str(event['payload']['connection_info']))
        #ts = datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%f")
        ts = datetime.strptime(event['@timestamp'][:26], '%Y-%m-%dT%H:%M:%S.%f')
        delay = event["payload"]["connection_info"]["delay"]
        delay = timedelta(seconds = delay)
        ts += delay
        sleep_time = ts - datetime.utcnow()

        if (sleep_time.days >= 0):
            self.registerTimer(sleep_time.seconds + sleep_time.microseconds / 10e6, self.evaluateNeuron, {"weight": event["payload"]["connection_info"]["weight"], "input_arival_time": ts})
        else:
            print("Deadline passed!")
            self.evaluateNeuron(weight = event["payload"]["connection_info"]["weight"], input_arival_time = ts)

    def loopForever(self):
        self.parameters["voltage_potential"] += self.parameters["current"]
        self.parameters["current"] = 0
        if (self.parameters["voltage_potential"] >= self.parameters["threshold"]):
            self.parameters["voltage_potential"] -= self.parameters["threshold"] - self.parameters["rest_potential"]
            spike = dict()
            spike['@timestamp'] = datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%S.%f")
            spike['payload'] = dict()
            if "population" in self.parameters:
                spike['payload']['population'] = self.parameters['population']
            self.sendSpike(spike)

        elif (self.parameters["voltage_potential"] < self.parameters["rest_potential"]):
            self.parameters["voltage_potential"] = self.parameters["rest_potential"]
        time.sleep(0.001)
