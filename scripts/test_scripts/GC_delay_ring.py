#!/usr/bin/env python
# -*- coding: utf-8 -*-

from GlobalController import *
import time 

neurons_in_ring = 3
neuron_type = 'LIFNeuron'
neuron_parameters = {"voltage_potential":0.0, "threshold":1.0, "noise_gain":0.1, "decay":0.5}
weight = 1.0
delay = 1.0

gc = GlobalController(servers='kafka')
#gc.dryrun = True

neuron_ids = range(1,neurons_in_ring+1)

print("Adding neurons...")
for neuron_id in neuron_ids:
    gc.addNeuroProgram(neuron_id, neuron_type)
    gc.setParameters(neuron_id, payload=neuron_parameters)
gc.flush()
time.sleep(1)

print("Connecting neurons...")
for neuron_id in neuron_ids:
    source = neuron_id
    destination = neuron_id + 1
    if destination == neurons_in_ring+1:
        destination = 1
    print(f"  Connecting {source} to {destination}")
    gc.connect(source, destination, weight=weight, delay=delay )
gc.flush()
time.sleep(1)

print("Sending initial spike")
gc.sendSpikeEvent(1)
gc.flush()
