#!/usr/bin/env python
# -*- coding: utf-8 -*-

from GlobalController import *

neuron_type = 'SimpleNeuron'
neuron_parameters = {"potential":0.0, "spike_value":1.0, "fraction":0.5, "period":0.5, "bias":1.0}
connection_info = {"weight":1.0}

gc = GlobalController()

gc.resetNeuroPrograms(1)
time.sleep(0.5)
gc.addNeuroProgram(1, neuron_type)
time.sleep(0.5)
gc.setParameters(1, neuron_parameters)
time.sleep(0.5)
gc.connect(2, 1, connection_info) #subscribe to spike_from_2
time.sleep(0.5)
gc.sendSpikeEvent(2)
