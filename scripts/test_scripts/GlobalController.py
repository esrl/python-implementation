#!/usr/bin/env python
# -*- coding: utf-8 -*-
import json,time
from datetime import datetime
from kafka import KafkaProducer

class GlobalController(object):
    def __init__(self, servers='kafka.cloudbrain.labnet'):
        self.servers = servers
        self.name = 'global_controller'
        self.dryrun = False
        self.producer = KafkaProducer(bootstrap_servers=self.servers)

    def __del__(self):
        self.flush()
    
    def flush(self):
        self.producer.flush()

    def sendEvent(self, sender, topic, payload):
        event = {
            "@timestamp": datetime.utcnow().isoformat(),
            "sender": sender,
            "payload": payload
        }
        if self.dryrun:
            print(json.dumps(event, indent=2))
        else:
            self.producer.send(topic, bytes(json.dumps(event),'utf-8'))

    def sendControlEvent(self, destination_id, payload):
        sender = self.name
        topic = 'control_to_' + str(destination_id)
        print("Sending " + payload['action'] + " control event from " + sender + " to " + topic)
        self.sendEvent(sender=sender, topic=topic, payload=payload)

    def sendSpikeEvent(self, source):
        sender = 'spike_from_' + str(source)
        print("Sending spike event on " + sender )
        self.sendEvent(sender=sender, topic=sender, payload={})

    def resetNeuroPrograms(self, destination_id):
        payload = {"action": "reset_neuro_programs"}
        self.sendControlEvent(destination_id=destination_id, payload=payload)

    def addNeuroProgram(self, destination_id, neuro_program):
        payload = {
            "action": "add_neuro_program",
            "neuro_program": neuro_program,
            "id": destination_id
        }
        self.sendControlEvent(destination_id=destination_id, payload=payload)

    def setParameters(self, destination_id, **payload):
        payload["action"] = "set_parameters"
        self.sendControlEvent(destination_id=destination_id, payload=payload)

    def connect(self, source_id, destination_id, **connection_info):
        payload = {
            "action": "subscribe",
            "topic": "spike_from_" + str(source_id),
            "connection_info": connection_info
        }
        self.sendControlEvent(destination_id=destination_id, payload=payload)
        print("Connecting " + str(source_id) + " to " + str(destination_id))
    
    def disconnect(self, source_id, destination_id):
        payload = {
            "action": "unsubscribe",
            "topic": "spike_from_" + str(source_id)
        }
        self.sendControlEvent(destination_id=destination_id, payload=payload)
        print("Disconnecting " + str(source_id) + " from " + str(destination_id))
