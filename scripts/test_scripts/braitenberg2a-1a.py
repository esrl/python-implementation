from GlobalController import *

if __name__ == "__main__":

    N = 5 # population size
    delay = 0.001
    
    threshold = 100

    exc_w = threshold / N # excitatory weights
    inh_w = -threshold / N * 2 # inhibitory weights

    a = [i + 1 for i in range(N * 0, N * 1)] 
    b = [i + 1 for i in range(N * 1, N * 2)] 
    c = [i + 1 for i in range(N * 2, N * 3)] 
    d = [i + 1 for i in range(N * 3, N * 4)] 

    gc = GlobalController()

    time.sleep(1)

    print("GC: Changing connections:")
    for i in range(N):

        # internal connections
        for j in range(N):
    
            # Disconnect A -> D
            gc.disconnect(a[i], d[j])
            # Connect A -> C
            gc.connect(a[i], c[j], weight = exc_w, delay = delay)

            # Disconnect B -> C
            gc.disconnect(b[i], c[j])
            # Connect B -> D
            gc.connect(b[i], d[j], weight = exc_w, delay = delay)

    time.sleep(2.0)
