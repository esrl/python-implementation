from GlobalController import *

if __name__ == "__main__":

    N = 5 # population size
    threshold = 100
    voltage_potential = 0
    rest_potential = 0
    noise = 0.10
    delay = 0.001

    exc_w = threshold / N # excitatory weights
    inh_w = -threshold / N * 2 # inhibitory weights
    noi_p_w = threshold * noise # inhibitory weights
    noi_n_w = -threshold * noise # inhibitory weights
    sen_w = threshold / N * 2

    a = [i + 1 for i in range(N * 0, N * 1)]
    b = [i + 1 for i in range(N * 1, N * 2)]
    c = [i + 1 for i in range(N * 2, N * 3)]
    d = [i + 1 for i in range(N * 3, N * 4)]
    e = [i + 1 for i in range(N * 4, N * 5)] 
    f = [i + 1 for i in range(N * 5, N * 6)] 
    g = [i + 1 for i in range(N * 6, N * 7)] 

    gc = GlobalController()

    time.sleep(1)

    for i in range(7 * N): 
        gc.resetNeuroPrograms(i + 1)

    time.sleep(5)

    # Setup 7 populations of N neurons
    print("GC: Setting neurons:")
    for i in range(N):
    
        # Adding neurons
        gc.addNeuroProgram(a[i], "IntegrateAndFireNeuron")
        gc.addNeuroProgram(b[i], "IntegrateAndFireNeuron")
        gc.addNeuroProgram(c[i], "IntegrateAndFireNeuron")
        gc.addNeuroProgram(d[i], "IntegrateAndFireNeuron")
        gc.addNeuroProgram(e[i], "NoisySpikeSource")
        gc.addNeuroProgram(f[i], "NoisySpikeSource")
        gc.addNeuroProgram(g[i], "NoisySpikeSource")
    
        # Setup neuron A
        gc.setParameters(a[i], population = "a", voltage_potential = 0, rest_potential = 0, threshold = threshold)
        # Setup neuron B
        gc.setParameters(b[i], population = "b", voltage_potential = 0, rest_potential = 0, threshold = threshold)    
        # Setup neuron C
        gc.setParameters(c[i], population = "c", voltage_potential = 0, rest_potential = 0, threshold = threshold)
        # Setup neuron D
        gc.setParameters(d[i], population = "d", voltage_potential = 0, rest_potential = 0, threshold = threshold)
        # Setup neuron E
        gc.setParameters(e[i], population = "e", min_time = 0.009, max_time = 0.011)
        # Setup neuron F
        gc.setParameters(f[i], population = "f", min_time = 0.01, max_time = 0.10)
        # Setup neuron G
        gc.setParameters(g[i], population = "g", min_time = 0.01, max_time = 0.10)
    
    print("GC: Setting up connections:")
    for i in range(N):

        # output connections
        # This is fixed by a hack in the frobit code.
    
        # internal connections
        for j in range(N):
    
            # input connections
            gc.connect("left_sensor_" + str(j), a[i], weight = sen_w, delay = delay)
            gc.connect("right_sensor_" + str(j), b[i], weight = sen_w, delay = delay)

            # A -> D
            gc.connect(a[i], d[j], weight = inh_w, delay = delay)
            # B -> C
            gc.connect(b[i], c[j], weight = inh_w, delay = delay)
            # E -> C
            gc.connect(e[i], c[j], weight = exc_w, delay = delay)
            # E -> D
            gc.connect(e[i], d[j], weight = exc_w, delay = delay)
    
        # F & G -> A
        gc.connect(f[i], a[i], weight = noi_p_w, delay = delay)
        gc.connect(g[i], a[i], weight = noi_n_w, delay = delay)
        # F & G -> B
        gc.connect(f[i], b[i], weight = noi_p_w, delay = delay)
        gc.connect(g[i], b[i], weight = noi_n_w, delay = delay)
        # F & G -> C
        gc.connect(f[i], c[i], weight = noi_p_w, delay = delay)
        gc.connect(g[i], c[i], weight = noi_n_w, delay = delay)
        # F & G -> D
        gc.connect(f[i], d[i], weight = noi_p_w, delay = delay)
        gc.connect(g[i], d[i], weight = noi_n_w, delay = delay)
        # F & G -> E
        gc.connect(f[i], e[i], weight = noi_p_w, delay = delay)
        gc.connect(g[i], e[i], weight = noi_n_w, delay = delay)
    
    time.sleep(2.0)
