#!/usr/bin/env python
# -*- coding: utf-8 -*-
import json, time
from datetime import datetime
from kafka import KafkaProducer

if __name__ == "__main__":
    p = KafkaProducer(bootstrap_servers='cloudbraink01')
   
    print("GC: Setting parameters")
    p.send('control_to_00', bytes(json.dumps({
        "@timestamp":datetime.utcnow().isoformat(), 
        "sender":"global_controller",
        "payload":{
            "action":"set_parameters",
            "id":"left",
            "send_topic":'motor_left'
            }
        } ),'utf-8'))

    p.send('control_to_00', bytes(json.dumps({
        "@timestamp":datetime.utcnow().isoformat(), 
        "sender":"global_controller",
        "payload":{
            "action":"set_parameters",
            "id":"right",
            "send_topic":'motor_right'
            }
        } ),'utf-8'))
    

    time.sleep(2.0)

