#!/usr/bin/env python
# -*- coding: utf-8 -*-
from kafka import KafkaProducer
from kafka_messages import *

p = KafkaProducer(bootstrap_servers='cloudbraink01')

print("Add and connect")
addNeuron(p, 200, "TestNeuron")
time.sleep(1.0)
connectNeurons(p, source=10, target=200, weight=1.0, delay=1.0)
time.sleep(1.0)

print("Send spikes")
for i in range(5):
    sendSpike(p, 10)
    time.sleep(1.0)

print("Reset neuron")
resetNeurons(p, 200)

print("Add and connect")
addNeuron(p, 200, "TestNeuron")
time.sleep(1.0)
connectNeurons(p, source=10, target=200, weight=1.0, delay=1.0)
time.sleep(1.0)

print("Send spikes")
for i in range(5):
    sendSpike(p, 10)
    time.sleep(1.0)


