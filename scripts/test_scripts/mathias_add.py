#!/usr/bin/env python
# -*- coding: utf-8 -*-

from kafka import KafkaProducer
from kafka_messages import *

p = KafkaProducer(bootstrap_servers='cloudbraink01')

number_of_neurons = 10

for i in range(number_of_neurons):
    addNeuron(p, i, "NoisySpikeSource")
    setParameters(p, i+1, min_time = 0.01, max_time = 0.001)
    time.sleep(0.2)
