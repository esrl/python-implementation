from GlobalController import *

if __name__ == "__main__":

    threshold = -0.055
    weight = 0.001
    delay = 0.001

    a = 1
    b = 2
    c = 3
    d = 4
    e = 5

    gc = GlobalController(servers='kafka')

    time.sleep(1)

    gc.addNeuroProgram(a, "NoisySpikeSource")
    gc.addNeuroProgram(b, "IFNeuron")
    gc.addNeuroProgram(c, "LIFNeuron")
    gc.addNeuroProgram(d, "AdExNeuron")
    
    time.sleep(1)
    
    gc.setParameters(a, population = "a", min_time = 1.0, max_time = 1.0)
    gc.setParameters(b, population = "b", voltage_potential = -0.070, rest_potential = -0.070, threshold = -0.055)
    gc.setParameters(c, population = "c", voltage_potential = -0.070, rest_potential = -0.070, threshold = -0.055, decay = 0.999)
    gc.setParameters(d, population = "d", voltage_potential = -0.070, rest_potential = -0.070, threshold = -0.055)

    time.sleep(1)
    
    gc.connect(a, b, weight = weight, delay = delay)    
    gc.connect(a, c, weight = weight, delay = delay)    
    gc.connect(a, d, weight = weight, delay = delay)    

    time.sleep(350000)

    gc.resetNeuroPrograms(a)
    gc.resetNeuroPrograms(b)
    gc.resetNeuroPrograms(c)
    gc.resetNeuroPrograms(d)
    
    time.sleep(1)
