#!/usr/bin/env python
# -*- coding: utf-8 -*-
import json, time
from datetime import datetime
from kafka import KafkaProducer
import random

from kafka_messages import *

if __name__ == "__main__":
    p = KafkaProducer(bootstrap_servers='kafka:9092')

    time.sleep(1)

    print("GC: Resetting neurons:")
    resetNeurons(p, 1)
    resetNeurons(p, 2)

    print("GC: Setting up neurons:")
    addNeuron(p, 1, "NoisySpikeSource")
    setParameters(p, 1, min_time = 0.01, max_time = 0.01)
    addNeuron(p, 2, "IntegrateAndFireNeuron")
    setParameters(p, 2, voltage_potential = 0, rest_potential = 0, threshold = 1)
    
    print("GC: Setting up connections:")
    connectNeurons(p, 1, 2, 1, 0.001)
    
    time.sleep(2)
