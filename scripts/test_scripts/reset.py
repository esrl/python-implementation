#!/usr/bin/env python
# -*- coding: utf-8 -*-
import json, time
from datetime import datetime
from kafka import KafkaProducer
import random

N = 1 # population size
exc_w = 1 # excitatory weights
inh_w = 1 # inhibitory weights
noi_w = 1 # inhibitory weights
p_connect = 0.25 # probability of connection between two neurons
delay = 0.001
spike_rate = 10

def resetNeurons(p, name):
    print("GC: Resetting neuron program:", name)
    p.send(name, bytes(json.dumps({
        "@timestamp":datetime.utcnow().isoformat(), 
        "sender":"global_controller",
        "payload":{
            "action":"reset_neuro_programs",
            }
        } ),'utf-8'))

if __name__ == "__main__":
    p = KafkaProducer(bootstrap_servers='cloudbraink01')

    print("GC: Restting all neurons")
    for i in range(6):
        resetNeurons(p, "control_to_" + str(i))

    time.sleep(2)
