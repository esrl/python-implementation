from kafka import KafkaProducer
import time
import json
from datetime import datetime 
p = KafkaProducer(bootstrap_servers='cloudbraink01')
for i in range(16):
    p.send('control_to_' + str(64 + i), bytes(json.dumps({
    "@timestamp": datetime.utcnow().isoformat(), 
    "sender":"tiger",
    "payload":{
        "action":"subscribe",
         "topic":"spike_from_tiger",
        "connection_info":{
             "weight": 1000000,
            "delay": 0.001
        }
    }
    } ),'utf-8'))
while True:
    p.send("spike_from_tiger", bytes(json.dumps({
    "@timestamp": datetime.utcnow().isoformat(), 
    "sender": "spike_from_tiger",
    "payload":{}
    } ),'utf-8'))
    print("send spike")
    time.sleep(0.01)
