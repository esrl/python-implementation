from GlobalController import *

if __name__ == "__main__":

    threshold = 5.0
    weight = 1.
    delay = 0.1

    a = 1
    b = 2
    c = 3

    gc = GlobalController(servers='kafka')

    time.sleep(1)

    # Setup 7 populations of N neurons
    print("GC: Setting neurons:")
    
    # Adding neurons
    gc.addNeuroProgram(a, "NoisySpikeSource")
    gc.addNeuroProgram(b, "IFNeuron")
    gc.addNeuroProgram(c, "IFNeuron")
    
    # Setup neuron A
    gc.setParameters(a, population = "a", min_time = .1, max_time = .1)
    # Setup neuron B
    gc.setParameters(b, population = "b", voltage_potential = 0, rest_potential = 0, threshold = threshold)
    # Setup neuron C
    gc.setParameters(c, population = "c", voltage_potential = 0, rest_potential = 0, threshold = threshold)    

    time.sleep(5)
    
    print("GC: Connecting neuron A to B")
    gc.connect(a, b, weight = weight, delay = delay)    
    time.sleep(10)

    print("GC: Disconnecting neuron A from B")
    gc.disconnect(a, b)
    time.sleep(5)

    print("GC: Connecting neuron A to C")
    gc.connect(a, c, weight = weight, delay = delay)
    time.sleep(10)

    print("GC: Disconnecting neuron A from C")
    gc.disconnect(a, c)
    time.sleep(5)

    gc.resetNeuroPrograms(a)
    gc.resetNeuroPrograms(b)
    gc.resetNeuroPrograms(c)

    time.sleep(2.0)
