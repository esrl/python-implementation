#!/usr/bin/env python
# -*- coding: utf-8 -*-
import json, time
from kafka import KafkaProducer

if __name__ == "__main__":
    p = KafkaProducer(bootstrap_servers='cloudbraink01')
   
    print("GC: Setting parameters")
    p.send('control_to_00', bytes(json.dumps({
        "@timestamp":"2019-12-05T06:11:27", 
        "sender":"global_controller",
        "payload":{
            }
        } ),'utf-8'))

    time.sleep(2.0)
    
    print("GC: Setting up network")
    p.send('control_to_00', bytes(json.dumps({
        "@timestamp":"2019-12-05T06:11:27", 
        "sender":"global_controller",
        "payload":{
            "action":"subscribe",
            "topic":"spikes_from_01",
            "connection_info":{"msg":"another spike from 01"}
            }
        } ),'utf-8'))

    time.sleep(2.0)

